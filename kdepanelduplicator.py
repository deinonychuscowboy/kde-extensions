#!/usr/bin/env python3
import configparser, sys, os, subprocess, time
from typing import *

panel_ids=set()

def parse_config(path:str="~/.config/plasma-org.kde.plasma.desktop-appletsrc")->dict:
	path:str=os.path.expanduser(path)
	conf:configparser.ConfigParser=configparser.ConfigParser()
	conf.optionxform=str # preserve case
	conf.read(path)
	cdict={}
	for k,v in conf.items():
		obj=cdict
		s=k.split("][")
		while len(s)>0:
			k2=s.pop(0)
			if k2 not in obj:
				obj[k2]={}
			obj=obj[k2]
		for k2,v2 in v.items():
			obj[k2]=v2
	return cdict

def save_config(cdict:dict,path:str="~/.config/plasma-org.kde.plasma.desktop-appletsrc")->None:
	path:str=os.path.expanduser(path)
	conf:configparser.ConfigParser=configparser.ConfigParser()
	conf.optionxform=str # preserve case
	while dict_in_values(cdict):
		cdict=flatten_dict(cdict)
	for k,v in cdict.items():
		if len(v)>0:
			conf[k]={}
			for k2,v2 in v.items():
				conf[k][k2]=v2
	with open(path,"w") as f:
		conf.write(f)
	conf:configparser.ConfigParser=configparser.ConfigParser()
	conf.optionxform=str # preserve case
	conf.read(os.path.expanduser("~/.config/plasmashellrc"))
	with open(os.path.expanduser("~/.config/plasmashellrc"),"w") as f:
		for x in panel_ids:
			conf["PlasmaViews][Panel "+str(x)+"][Defaults"]={}
			conf["PlasmaViews][Panel "+str(x)+"][Defaults"]["thickness"]="24"
		conf.write(f)
	pass

def dict_in_values(conf:dict)->bool:
	for k,v in conf.items():
		for k2,v2 in v.items():
			if isinstance(v2,dict):
				return True

def flatten_dict(conf:dict)->dict:
	cnew={}
	for k,v in conf.items():
		cnew[k]={}
		dicts={}
		for k2,v2 in v.items():
			if isinstance(v2,dict):
				dicts[k+"]["+k2]=v2
			else:
				cnew[k][k2]=v2
		for k2,v2 in dicts.items():
			cnew[k2]=v2
	return cnew

def clone_entry(src_root:Dict,dest_root:Dict,src_id:str,dest_id:str,id_counter:int)->Dict:
	src:Dict=src_root[src_id]
	dest_root[dest_id]={}
	dest:Dict=dest_root[dest_id]
	for k,v in src.items():
		if not isinstance(v, Iterable) or isinstance(v, str) or isinstance(v, bytes) or isinstance(v, bytearray):
			dest[k]=v
		else:
			if k.isnumeric():
				id_counter+=1
				dest=clone_entry(src,dest,k,str(id_counter),id_counter)
			else:
				dest=clone_entry(src,dest,k,k,id_counter)
	return dest_root

def clone_first_panel(conf:dict,monitor:int)->dict:
	newid=get_new_id(conf)
	first_panel_id=get_panel_id(conf)
	conf["Containments"]=clone_entry(conf["Containments"],conf["Containments"],first_panel_id,newid,int(newid))
	panel_ids.add(newid)
	panel_ids.add(first_panel_id)

	applet_mapping={}
	counter=0
	for k in conf["Containments"][first_panel_id]["Applets"]:
		applet_mapping[k]=tuple(conf["Containments"][newid]["Applets"].keys())[counter]
		counter+=1

	conf["Containments"][newid]["General"]["AppletOrder"]=[]
	for id in conf["Containments"][first_panel_id]["General"]["AppletOrder"].split(";"):
		conf["Containments"][newid]["General"]["AppletOrder"].append(applet_mapping[id])
	conf["Containments"][newid]["General"]["AppletOrder"]=";".join(conf["Containments"][newid]["General"]["AppletOrder"])

	conf["Containments"][newid]["lastScreen"]=monitor

	return conf

def clone_first_tray(conf:dict,monitor:int)->dict:
	newid=get_new_id(conf)
	conf["Containments"]=clone_entry(conf["Containments"],conf["Containments"],get_tray_id(conf),newid,int(newid))
	conf["Containments"][newid]["lastScreen"]=monitor
	return conf

def clone_first_desktop(conf:dict,monitor:int)->dict:
	newid=get_new_desktop_id(conf)
	conf["Containments"]=clone_entry(conf["Containments"],conf["Containments"],get_desktop_id(conf),newid,int(newid))
	conf["Containments"][newid]["lastScreen"]=monitor
	return conf

def clone_first_actions(conf:dict,monitor:int)->dict:
	newid=get_new_actions_id(conf)
	conf["ActionPlugins"]=clone_entry(conf["ActionPlugins"],conf["ActionPlugins"],get_actions_id(conf),newid,int(newid))
	return conf

def delete_extra_panels(conf:dict)->dict:
	panels:List=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.panel":
			panels.append(k)
	conf["Containments"][panels.pop(0)]["lastScreen"]=str(0)
	for k in panels:
		del conf["Containments"][k]
	return conf

def delete_extra_trays(conf:dict)->dict:
	trays:List=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.private.systemtray":
			trays.append(k)
	conf["Containments"][trays.pop(0)]["lastScreen"]=str(0)
	for k in trays:
		del conf["Containments"][k]
	return conf

def delete_extra_desktops(conf:dict)->dict:
	desktop_sets:List[str]=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.folder": # not compatible with other settings?
			desktop_sets.append(k)
	conf["Containments"][desktop_sets.pop(0)]["lastScreen"]=str(0)
	for k in desktop_sets:
		del conf["Containments"][k]
	return conf

def delete_extra_actions(conf:dict)->dict:
	action_sets:List[str]=[]
	for k,v in conf["ActionPlugins"].items():
		action_sets.append(k)
	action_sets.pop(0)
	for k in action_sets:
		del conf["ActionPlugins"][k]
	return conf

def get_panel_id(conf:dict)->str:
	panels:List=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.panel":
			panels.append(k)
	return panels.pop(0)

def get_tray_id(conf:dict)->str:
	trays:List=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.private.systemtray":
			trays.append(k)
	return trays.pop(0)

def get_desktop_id(conf:dict)->int:
	desktop_sets:List[str]=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.folder": # not compatible with other settings?
			desktop_sets.append(k)
	return desktop_sets.pop(0)

def get_actions_id(conf:dict)->int:
	action_sets:List[str]=[]
	for k,v in conf["ActionPlugins"].items():
		action_sets.append(k)
	return action_sets.pop(0)

def get_new_id(conf:dict)->str:
	max=0
	for k,v in conf["Containments"].items():
		if k.isnumeric() and int(k)>max:
			max=int(k)
		if "Applets" in v:
			for k2,v2 in v["Applets"].items():
				if k2.isnumeric() and int(k2)>max:
					max=int(k2)
	return str(max+1)

def get_new_desktop_id(conf:dict)->str:
	max=0
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.folder": # not compatible with other settings?
			if k.isnumeric() and int(k)>max:
				max=int(k)
	return str(max+1)

def get_new_actions_id(conf:dict)->str:
	max=0
	for k,v in conf["ActionPlugins"].items():
		if k.isnumeric() and int(k)>max:
			max=int(k)
	return str(max+1)

def get_num_monitors(conf:dict)->int:
	desktop_sets:List[str]=[]
	for k,v in conf["Containments"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.folder": # not compatible with other settings?
			desktop_sets.append(k)
	return len(desktop_sets)

def set_tray_id(panel:dict,tray_id:str)->None:
	for k,v in panel["Applets"].items():
		if "plugin" in v and v["plugin"]=="org.kde.plasma.systemtray":
			v["Configuration"]["SystrayContainmentId"]=tray_id

if __name__ == "__main__":
	subprocess.run("(plasmashell --replace &); disown; exit",shell=True,stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
	print("Restarting plasma to be sure config file is up to date...")
	time.sleep(10)
	print("Applying config changes...")
	conf=parse_config()
	num_monitors=get_num_monitors(conf)
	conf=delete_extra_panels(conf)
	conf=delete_extra_trays(conf)
	conf=delete_extra_desktops(conf)
	conf=delete_extra_actions(conf)
	for i in range(1,num_monitors):
		conf=clone_first_desktop(conf,str(i))
		conf=clone_first_actions(conf,str(i))
		conf=clone_first_panel(conf,str(i))
		panel=conf["Containments"][tuple(conf["Containments"].keys())[-1]]
		conf=clone_first_tray(conf,str(i))
		tray_id=tuple(conf["Containments"].keys())[-1]
		set_tray_id(panel,tray_id)
	save_config(conf)
	print("Loading new config file...")
	subprocess.run("plasmashell --replace & disown; exit",shell=True,stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
	time.sleep(5)

# TODO cleanup passbacks
