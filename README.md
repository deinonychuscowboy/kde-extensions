KDE Extensions
==============

These extensions to KDE/Plasma add basic features to the desktop environment that are not yet supported out of the box.

kdepanelduplicator -- Display the same panel on multiple monitors by copying it. Will grab your first panel and copy it to all your monitors.
sync-desktop-wallpaper -- Apply the same wallpaper to all monitors, useful when plasma gets confused and resets a monitor to the default. Prefers custom wallpapers to built-in ones.
