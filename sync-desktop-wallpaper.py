#!/usr/bin/env python3
import configparser, sys, os, subprocess, time
from typing import *

def parse_config(path:str="~/.config/plasma-org.kde.plasma.desktop-appletsrc")->dict:
	path:str=os.path.expanduser(path)
	conf:configparser.ConfigParser=configparser.ConfigParser()
	conf.optionxform=str # preserve case
	conf.read(path)
	cdict={}
	for k,v in conf.items():
		obj=cdict
		s=k.split("][")
		while len(s)>0:
			k2=s.pop(0)
			if k2 not in obj:
				obj[k2]={}
			obj=obj[k2]
		for k2,v2 in v.items():
			obj[k2]=v2
	return cdict

def save_config(cdict:dict,path:str="~/.config/plasma-org.kde.plasma.desktop-appletsrc")->None:
	path:str=os.path.expanduser(path)
	conf:configparser.ConfigParser=configparser.ConfigParser()
	conf.optionxform=str # preserve case
	while dict_in_values(cdict):
		cdict=flatten_dict(cdict)
	for k,v in cdict.items():
		if len(v)>0:
			conf[k]={}
			for k2,v2 in v.items():
				conf[k][k2]=v2
	with open(path,"w") as f:
		conf.write(f)
	pass

def dict_in_values(conf:dict)->bool:
	for k,v in conf.items():
		for k2,v2 in v.items():
			if isinstance(v2,dict):
				return True

def flatten_dict(conf:dict)->dict:
	cnew={}
	for k,v in conf.items():
		cnew[k]={}
		dicts={}
		for k2,v2 in v.items():
			if isinstance(v2,dict):
				dicts[k+"]["+k2]=v2
			else:
				cnew[k][k2]=v2
		for k2,v2 in dicts.items():
			cnew[k2]=v2
	return cnew

def sync_desktop_wallpaper(conf:dict)->dict:
	wallpaper:Union[str,None]=None
	desktop_sets:List[str]=[]
	for k,v in conf["Containments"].items():
		if "Wallpaper" in v:
			desktop_sets.append(k)
			if wallpaper is None or wallpaper.startswith("file:///usr/share") and not v["Wallpaper"][v["wallpaperplugin"]]["General"]["Image"].startswith("file:///usr/share"):
				wallpaper=v["Wallpaper"][v["wallpaperplugin"]]["General"]["Image"]
	print("Setting wallpaper to "+wallpaper)
	for k in desktop_sets:
		conf["Containments"][k]["Wallpaper"][conf["Containments"][k]["wallpaperplugin"]]["General"]["Image"]=wallpaper
	return conf

if __name__ == "__main__":
	print("Applying config changes...")
	conf=parse_config()
	conf=sync_desktop_wallpaper(conf)
	save_config(conf)
	print("Loading new config file...")
	subprocess.run("plasmashell --replace & disown; exit",shell=True,stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
	time.sleep(5)
